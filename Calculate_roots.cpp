#include <windows.h>
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    string answer;
    int a;

    cout << "Hi, do you want to know the square root of the number you give us? " << endl;
    cin >> answer;

    if (answer == "yes")
    {
        cout << "Ok, go on" << endl;
        cout << "Enter the number from which you want to calculate the square root" << endl;
        cin >> a;
        if (a >= 0)
        {
            double o = sqrt(double(a));
            cout << "Square root of a number " << a << " is " << o << endl;
        }
        cout << "Do you want to continue? " << endl;
        cin >> answer;
    }

    if (answer == "no")
    {
        cout << "so we can't help you, sorry" << endl;
    }
    return 0;
}
